import { readFileSync } from "fs";
import { uploadFile } from "./file.js";
import mime from "mime";

let totalWasted = 0;

const uploadTasks = async (filePath: string) => {
  const buffer = readFileSync(filePath); // Read the file into a buffer
  const size = buffer.byteLength;
  const mimeType = mime.getType(filePath);

  const fileName = filePath.split(/[\/\\]/).pop(); // Use a regex to split on either / or \

  const tasks = Array.from({ length: 10 }, async (_, index) => {
    const id = await uploadFile(
      "attachments",
      {
        name: fileName ?? "",
        file: buffer,
      },
      mimeType ?? ""
    );
    totalWasted += size;

    console.log("ok lmao", id);
  });

  await Promise.all(tasks);

  console.log(
    `You've wasted ${totalWasted / 1000} kilobytes of tates bandwidth!`
  );
};

// Get file path from command line arguments
const filePath = process.argv[2];

// If no file path is provided, exit the script
if (!filePath) {
  console.error("No file path provided.");
  process.exit(1);
}

// Run the tasks every 10 seconds
setInterval(() => uploadTasks(filePath), 10000);
