import axios from "axios";
import FormData from "form-data";

const AutumnURL = "https://files2.therealworld.ag";

export async function uploadFile(
  type: string,
  file: { name: string; file: Buffer },
  mimeType: string
) {
  const data = new FormData();
  data.append("file", file.file, {
    filename: file.name,
    contentType: mimeType,
  });

  const response = await axios.post(`${AutumnURL}/${type}`, data, {
    headers: data.getHeaders(),
  });

  return response.data.id;
}
